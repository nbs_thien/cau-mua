/**
 * @class User
 */
var Schema, model, mongoose, _schema;
mongoose = require('mongoose');
Schema = mongoose.Schema;

_schema = new Schema({
  userID: {
    type: String,
    "default": ''
  },
  display_name: {
    type: String,
    "default": ''
  },
  created_time: {
    type: Date,
    "default": Date.now()
  }
}, {
  strict: false
});

model = function() {
  var _createNew, _model;
  _createNew = function(data, callback) {
    var new_user;
    new_user = new _model({
      userID: data.userID,
      display_name: data.display_name
    });

    return new_user.save((function(_this) {
      return function(err) {
        if (err != null) {
          callback(err, null);
        }
        return callback(null, new_user);
      };
    })(this));
  };
  // _schema.methods = {};
  _model = mongoose.model('User', _schema);
  return {
    schema: _schema,
    model: _model,
    createNew: _createNew
  };
};

module.exports = model();
