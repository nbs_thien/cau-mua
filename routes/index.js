var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/user');
var https = require('https');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// 
router.post('/login', function(req, res, next) {
	var url = 'https://graph.facebook.com/me?access_token=' + req.body.accessToken;
	console.log('https ', url);
	https.get(url, function(result) {
	    var responseParts = [];
	    result.setEncoding('utf8');
	    result.on("data", function(chunk) {
	        responseParts.push(chunk);
	        console.log(chunk);
	    });
	    result.on("end", function(){
	        var data = responseParts.join('');
	        data = JSON.parse(data);
			User.createNew({
				userID: data.id,
				display_name: data.name
			}, function(err) {
				if (err) {
					return res.send({
						code: -1
					})
				}
				res.send({
					code: 0,
					display_name: data.name,
					userID: data.id
				});	
			});
	        
    	});

		
	});
});



module.exports = router;
