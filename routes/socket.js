channels = global.channels;
rooms = global.rooms;

function createChannel(channel) {
  display_names = {};
  numUsers = 0;
  function updateRoom(c) {
    rooms[c] = {
      display_names: display_names,
      numUsers: numUsers
    }
  }

  updateRoom(channel);
  
  return function(socket) {
      var addedUser = false;
      console.log('someone connected');

      // when the client emits 'new message', this listens and executes
      socket.on('send', function (data) {
        socket.broadcast.emit('new message', {
          display_name: display_names[socket.userID],
          message: data
        });
      });

      // when the client emits 'add user', this listens and executes
      socket.on('login', function (data) {
        socket.userID = data.userID;
        display_names[data.userID] = data.display_name;
        ++numUsers;
        updateRoom(channel);

        addedUser = true;
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('user joined', {
          userID: socket.userID,
          numUsers: numUsers
        });
      });

      // when the user disconnects.. perform this
      socket.on('disconnect', function () {
        var _display_name = display_names[socket.userID] || "Vô Danh"
        
        if (addedUser) {
          delete display_names[socket.userID];
          --numUsers;
          updateRoom(channel);

          socket.broadcast.emit('user left', {
            userID: socket.userID,
            display_name: _display_name,
            numUsers: numUsers
          });
        }
      });
    }
}

module.exports = function(channel) {
  if (!channels[channel]) {
    channels[channel] = createChannel(channel);
  }
  return channels[channel];
}
